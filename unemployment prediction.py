# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 21:50:30 2023

@author: Karlo Kokanović
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pandas_datareader.data as web
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense,Dropout,LSTM
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import KFold, cross_val_score
from sklearn import ensemble


selectedOption = input("Select the number of Country you want to predict unemployment for:\n\t1. Croatia\n\t2. Austria\n\t3. Germany\n")
if(selectedOption == "1"):
    rememberGeo = "Croatian"
    geo = 'GEO=[HR]'
elif(selectedOption == "2"): 
    rememberGeo = "Austrian"
    geo = 'GEO=[AT]'
elif(selectedOption == "3"): 
    rememberGeo = "Germany"
    geo = 'GEO=[DE]'

selectedGender= input("Select the gender:\n\t1. Male\n\t2. Female\n")
if(selectedGender == "1"):
    rememberGender = "male"
    gender = 'SEX=[M]'
elif(selectedGender == "2"): 
    rememberGender = "female"
    gender = 'SEX=[F]'

#loading dataset
#80% of the 15 year period we're considering (192 months)    
dataset_train =web.DataReader( 
    '&'.join([
        'dataset=UNE_RT_M', 
        'from=2002-12-01', 
        'h=TIME', 
        'to=2018-11-01', 
        'v=Geopolitical entity (reporting)', 
        'AGE=[Y25-74]', 
        'FREQ=[M]', 
        geo,gender, 
        'S_ADJ=[SA]', 
        'UNIT=[PC_ACT]']),'econdb'
)
#Eurostat did not publish the info for December 2022, that's why November 2022 is last, remaining 20%
dataset_test = web.DataReader(
    '&'.join([
        'dataset=UNE_RT_M', 
        'from=2018-12-01', 
        'h=TIME', 
        'to=2023-01-01', 
        'v=Geopolitical entity (reporting)', 
        'AGE=[Y25-74]', 
        'FREQ=[M]', 
        geo,gender, 
        'S_ADJ=[SA]', 
        'UNIT=[PC_ACT]']),'econdb'
)

# #taking only percentages from the dataset
training_set = dataset_train.iloc[:,0:1].values

# #normalization
scaler = MinMaxScaler(feature_range = (0,1))
scaled_training_set = scaler.fit_transform(training_set)

# #preparing the data
x_train = []
y_train = []
for i in range(2,len(scaled_training_set)): 
    x_train.append(scaled_training_set[i-2:i,0])
    y_train.append(scaled_training_set[i,0])
x_train= np.array(x_train)
y_train = np.array(y_train)

# #reshaping the data
x_train, y_train = np.array(x_train), np.array(y_train)
x_train = np.reshape(x_train,(x_train.shape[0],x_train.shape[1],1))

# #building the models for LSTM
model = Sequential()
model.add(LSTM(units=50,return_sequences=True,input_shape = (x_train.shape[1],1)))
model.add(Dropout(0.2))
model.add(LSTM(units=50,return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=50,return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=50))
model.add(Dropout(0.2))
model.add(Dense(units=1))
model.summary()
model.compile(optimizer='adam',loss='mean_squared_error')
model.fit(x_train, y_train, epochs = 40, batch_size= 8, validation_split=0.2)

# #Percentages we will compare to
actual_percentage = dataset_test.iloc[:,0:1].values

# #prepare the input for the model
dataset_total = pd.concat((dataset_train['Monthly'],dataset_test['Monthly']),axis=0)
model_inputs = dataset_total[len(dataset_total)-len(dataset_test)-2:].values
model_inputs = model_inputs.reshape(-1,1)
model_inputs = scaler.transform(model_inputs)
x_test = []
for i in range(2,len(model_inputs)):
    x_test.append(model_inputs[i-2:i, 0])
x_test = np.array(x_test)
x_test = np.reshape(x_test,(x_test.shape[0],x_test.shape[1],1))

# #prediction with LSTM model
predicted_percentage = model.predict(x_test)
predicted_percentage = scaler.inverse_transform(predicted_percentage)

#Prepairing data for KNN and RF
#Introducing lag features
df = dataset_train.copy()
df['Time'] = df['Monthly'].shift(1)
df.fillna(0, inplace=True)
X = df.loc[:, ['Monthly']]  # features
X = np.array(X)
y = df.loc[:, 'Time']  # target
y = np.array(y)
X_test = dataset_test.loc[:, ['Monthly']]
X_test = np.array(X_test)
#Model and predictions
kf = KFold(n_splits=5, shuffle=True, random_state=42)

def rmse(score):
    rmse = np.sqrt(-score)
    print(f'rmse= {"{:.2f}".format(rmse)}')
score = cross_val_score(ensemble.RandomForestRegressor(random_state= 42), X, y, cv= kf, scoring="neg_mean_squared_error")

estimators = [50, 100, 150, 10, 20, 30, 40]

for count in estimators:
    score = cross_val_score(ensemble.RandomForestRegressor(n_estimators= count, random_state= 42), X, y, cv= kf, scoring="neg_mean_squared_error")
    print(f'For estimators: {count}')
    rmse(score.mean())

model_rf = RandomForestRegressor(n_estimators = 10)
model_rf.fit(X, y)
predictions_rf = model_rf.predict(X_test)

neigbours =  [2,3,4,5,6,7,8,9,10]
score_knn = cross_val_score(KNeighborsRegressor(), X, y, cv= kf, scoring="neg_mean_squared_error")
for count in neigbours:
    score = cross_val_score(KNeighborsRegressor(n_neighbors= count), X, y, cv= kf, scoring="neg_mean_squared_error")
    print(f'For neigbours: {count}')
    rmse(score.mean())
model_knn = KNeighborsRegressor(n_neighbors=3)
model_knn.fit(X, y)
predictions_knn = model_knn.predict(X_test)
actual_percentage = dataset_test.loc[:, ['Monthly']] 
actual_percentage = np.array(actual_percentage)

plt.plot(dataset_train, color="blue",label = "Training percentages")
plt.plot(dataset_test, color="red",label = "Test percentages")
plt.xlabel('Period')
plt.ylabel("Percentage of unemployment")
plt.legend()
plt.show()

plt.plot(actual_percentage, color="red",label = "Actual percentages")
plt.plot(predicted_percentage, color="blue",label = "Predicted percentages using LSTM")
plt.title(f"Prediction of unemployment for {rememberGeo} {rememberGender} (aged 25-74)")
plt.xlabel('Months')
plt.ylabel("Percentage of unemployment")
plt.legend()
plt.show()

plt.plot(actual_percentage, color="red",label = "Actual percentages")
plt.plot(predictions_rf, color="purple",label = "Predicted percentages using RF")
plt.title(f"Prediction of unemployment for {rememberGeo} {rememberGender} (aged 25-74)")
plt.xlabel('Months')
plt.ylabel("Percentage of unemployment")
plt.legend()
plt.show()

plt.plot(actual_percentage, color="red",label = "Actual percentages")
plt.plot(predictions_knn, color='green',label= 'Predicted percentages using KNN')
plt.title(f"Prediction of unemployment for {rememberGeo} {rememberGender} (aged 25-74)")
plt.xlabel('Months')
plt.ylabel("Percentage of unemployment")
plt.legend()
plt.show()

# predicting next month
next_month = [model_inputs[len(model_inputs)+1 - 2:len(model_inputs+1),0]]
next_month = np.array(next_month)
next_month = np.reshape(next_month,(next_month.shape[0],next_month.shape[1],1))
prediction = model.predict(next_month)
prediction = scaler.inverse_transform(prediction)
print("Next month prediction is: ", float(prediction))